FROM node:8.11-alpine
COPY package.json /cicd/package.json
RUN cd /cicd && npm install
COPY . /cicd/
WORKDIR /cicd
EXPOSE 80
CMD ["npm", "start"]

